//
//  codereader.h
//  codereader
//
//  Created by Luiz Fernando Aquino Dias on 07/02/17.
//  Copyright © 2017 Town Tree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZBarSDK.h"

@interface codereader : NSObject <ZBarReaderDelegate> {
    
    
}

@property (nonatomic,readonly) UIImage *originalImage;

- (id)initWithImage:(UIImage *)image;
- (UIImage *)grayScaleImage;
- (UIImage *)oldImageWithIntensity:(CGFloat)level;




@end
